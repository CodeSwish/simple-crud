import "./style.css";

const formSubmit = document.querySelector("form");

let selectedRow = null;

formSubmit.addEventListener("submit", (e) => {
  e.preventDefault();
  let formData = getVal();

  if (selectedRow == null) {
    displayVal(formData);
  } else {
    updateData(formData);
  }
  clearForm();
});

const getVal = () => {
  const nameEl = document.querySelector("[data-name]").value;
  const ageEl = document.querySelector("[data-age]").value;
  const genderEl = document.querySelector("[data-gender]").value;


  let formData = {
    name: nameEl,
    age: ageEl,
    gender: genderEl,
  };

  return formData;
};

const displayVal = (dataObj) => {
  let table = document.querySelector("table").getElementsByTagName("tbody")[0];

    const newRow = table.insertRow();
    let cell1 = newRow.insertCell(0);
    let cell2 = newRow.insertCell(1);
    let cell3 = newRow.insertCell(2);
    let cell4 = newRow.insertCell(3);

    cell1.innerHTML = dataObj.name;
    cell2.innerHTML = dataObj.age;
    cell3.innerHTML = dataObj.gender;

    cell4.innerHTML = `<button data-edit>Edit</button><button data-delete>Delete</button>`;
    const editBtn = newRow.querySelector("[data-edit]");
    const delBtn = newRow.querySelector("[data-delete]");

    editBtn.addEventListener("click", () => editData(newRow));
    delBtn.addEventListener("click", () => deleteData(newRow));
};

const editData = (row) => {
  selectedRow = row
  document.querySelector("[data-name]").value = row.cells[0].innerHTML;
  document.querySelector("[data-age]").value = row.cells[1].innerHTML;
  document.querySelector("[data-gender]").value = row.cells[2].innerHTML;
};

const updateData = (dataObj) => {
  selectedRow.cells[0].innerText = dataObj.name;
  selectedRow.cells[1].innerText = dataObj.age;
  selectedRow.cells[2].innerText = dataObj.gender;
};

const deleteData = (row) => {
  document.querySelector("#list").deleteRow(row.rowIndex)
};

const clearForm = () => {
  document.querySelector("[data-name]").value = "";
  document.querySelector("[data-age]").value = "";
  document.querySelector("[data-gender]").value = "";
  selectedRow = null;
};
